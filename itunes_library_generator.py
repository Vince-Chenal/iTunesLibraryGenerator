#!/usr/bin/python3
# -*- coding: utf-8 -*-
from pyItunes import *
import taglib
import sys, os
import argparse
import logging
import pathlib
import timeit

# Logging
logger = logging.getLogger(__name__)

# Global Variables
args = ""
current_id = 1
final_library_path = "/tmp/itunes_generated_library.xml"
final_library_path_windows = "/tmp/itunes_generated_library_win.xml"


def main():

    # Reading empty iTunes plist file
    library = Library("itunes_plist.xml")

    global args
    root_folder = args.root_folder
    logger.info("Root folder : "+root_folder)

    # Walking through root_folder
    for root, subdirs, files in os.walk(root_folder):

        for filename in files:
            # Compute absolute path
            absolute_file_path = os.path.join(root, filename)

            # If this is an mp3
            if(filename.endswith(".mp3")):
                global current_id

                logger.info('\t- file %s' % (filename))

                # Read mp3 tags
                song_tags = taglib.File(absolute_file_path).tags
                logger.debug(song_tags)

                # Create the new song to inject
                to_inject = {}
                to_inject['Track ID'] = str(current_id)

                # Track Artist
                if 'ARTIST' in song_tags:
                    if len(song_tags['ARTIST']) > 0:
                        to_inject['Artist'] = song_tags['ARTIST'][0]

                # Track Title
                if 'TITLE' in song_tags:
                    if len(song_tags['TITLE']) > 0:
                        to_inject['Name'] = song_tags['TITLE'][0]

                # Track Album
                if 'ALBUM' in song_tags:
                    if len(song_tags['ALBUM']) > 0:
                        to_inject['Album'] = song_tags['ALBUM'][0]

                # Track Genre
                if 'GENRE' in song_tags:
                    if len(song_tags['GENRE']) > 0:
                        to_inject['Genre'] = song_tags['GENRE'][0]

                # Track Album Artist
                if 'ALBUMARTIST' in song_tags:
                    if len(song_tags['ALBUMARTIST']) > 0:
                        to_inject['Album Artist'] = song_tags['ALBUMARTIST'][0]

                # Track Year
                if 'DATE' in song_tags:
                    if len(song_tags['DATE']) > 0:
                        to_inject['Year'] = song_tags['DATE'][0]

                # Track location
                to_inject['Location'] = pathlib.Path(absolute_file_path).as_uri()

                # Track number + count
                if 'TRACKNUMBER' in song_tags:
                    if len(song_tags['TRACKNUMBER']) > 0:
                        track_number = song_tags['TRACKNUMBER'][0]
                        if "/" in track_number:
                            track_count = track_number[track_number.index('/')+1:]
                            track_number = track_number[:track_number.index('/')]
                            to_inject['Track Number'] = track_number
                            to_inject['Track Count'] = track_count
                        else:
                            to_inject['Track Number'] = track_number

                # Date Added/Modified
                # TODO : date added/modified ? pas sur que ce soit utile iTunes arrive à les retrouver par lui même

                # Inject new_song into playlist
                library.il['Tracks'][to_inject['Track ID']] = to_inject
                library.il['Playlists'][0]['Playlist Items'].append({'Track ID': to_inject['Track ID']})
                library.il['Playlists'][1]['Playlist Items'].append({'Track ID': to_inject['Track ID']})

            # If this is a WAV file
            elif(filename.endswith("wav")):
                logger.error("WAV fileformat is not currently supported")

            # If this is a FLAC file
            elif (filename.endswith("flac")):
                logger.error("FLAC fileformat is not currently supported")

            current_id += 1

    library.writePlaylist(final_library_path)

    # Create windows version
    linux_version = open(final_library_path, "r+")
    content = linux_version.read()
    content = content.replace("file:///media/vince/SANDISK_VINCE/", "file://localhost/D:/")
    #quickdirty fix
    content = content.replace("file://localhost/D:localhost/C:/Users/Vince/Music/iTunes/iTunes%20Media/", "file://localhost/C:/Users/Vince/Music/iTunes/iTunes%20Media/")

    windows_version = open(final_library_path_windows, "w+")
    windows_version.write(content)
    linux_version.close()
    windows_version.close()
    '''
    # Display what's inside
    for id, song in l.songs.items():
        if song :
            print(song.name)

    playlists = l.getPlaylistNames()

    for song in l.getPlaylist(playlists[0]).tracks:
        print("\nplaylist : "+playlists[0])
        print("%s - %s" % (song.artist, song.name))
    '''

if __name__ == "__main__":
    # Get starting time
    start_time = timeit.default_timer()

    # Parse arguments
    parser = argparse.ArgumentParser()
    parser.add_argument("-rf", "--root-folder", type=str,
                        help="The root folder from where to start searching for songs", required=True)
    parser.add_argument("-d", "--debug", help="Enable debugging mode", action="store_true")

    global args
    args = parser.parse_args()

    # Configure logging
    logging.basicConfig(level=logging.ERROR, format="%(asctime)s :: %(levelname)s :: %(message)s")
    if args.debug:
        logging.getLogger(__name__).setLevel(logging.DEBUG)
        logger.debug("DEBUG mode enabled")
    else:
        logging.getLogger(__name__).setLevel(logging.ERROR)

    main()
    elapsed = timeit.default_timer() - start_time
    print(str(current_id)+" songs have been treated.")
    print("Execution took "+str(elapsed)+" seconds.")
    logger.info(str(current_id)+" songs have been treated.")
    logger.info("Execution took "+str(elapsed)+" seconds.")
